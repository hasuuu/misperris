from django.shortcuts import render
from django.core import serializers
from django.http import HttpResponse
import time
from django.shortcuts import render, get_object_or_404, redirect , render_to_response
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.core.mail import EmailMessage
from .forms import PerroForm, ComentarioForm
from . models import feed , Perro , Comentario
import json
# Create your views here.
def index(request):
	template='posts/index.html'
	results=Perro.objects.all()
	jsondata = serializers.serialize('json',results)
	context={
		'results':results,
		'jsondata':jsondata,
	}
	return render(request,template,context)

def getdata(request):
	results=Perro.objects.all()
	jsondata = serializers.serialize('json',results)
	return HttpResponse(jsondata)

def base_layout(request):
	template='posts/base.html'
	return render(request,template)

	    
def indexuser(request):
    fecha = time.strftime("%c")
    return render(request, 'index1.htm', {'fechaHora':fecha})
   
def form(request):
    fecha = time.strftime("%c")
    return render(request, 'form.html', {'fechaHora':fecha})

def indexadmin(request):
    fecha = time.strftime("%c")
    return render(request, 'index2.htm', {'fechaHora':fecha})


def ingresar(request):
    
    if request.method == 'POST':
        formulario = AuthenticationForm(request.POST)
        if formulario.is_valid:
            usuario = request.POST['username']
            clave = request.POST['password']
            acceso = authenticate(username=usuario, password=clave)
            if acceso is not None:
                if acceso.is_active:
                    login(request, acceso)
                    return HttpResponseRedirect('/cliente/privado')
                else:
                    return render(request, 'noactivo.html')
            else:
                return render(request, 'nousuario.html')
    else:
        formulario = AuthenticationForm()
        context = {'formulario': formulario}
        return render(request, 'ingresar.html', context)

@login_required(login_url='/cliente/ingresar')
def privado(request):
    if request.user.is_staff:
        usuario = request.user
        context = {'usuario': usuario}
        return render(request, 'index2.htm', context)
    if request.user.is_staff == False:
        usuario = request.user
        context = {'usuario': usuario}
        return render(request, 'index1.htm', context)

def usuario_nuevo(request):
    if request.method=='POST':
        formulario = UserCreationForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return HttpResponseRedirect('/')
    else:
        formulario = UserCreationForm()
    context = {'formulario': formulario}
    return render(request, 'nuevousuario.html', context)

def Perro_nuevo(request):
    if request.method=='POST':
        formulario = PerroForm(request.POST, request.FILES)
        if formulario.is_valid():
            formulario.save()
            return HttpResponseRedirect('/perros')
    else:
        formulario = PerroForm()
    context = {'formulario': formulario}
    return render(request, 'perro_perrosform.html', context)

def perros(request):
    perros = Perro.objects.all()
    context = {'datos': perros}
    return render(request, 'perros_lista_perros.html', context)

def perros2(request):
    perros = Perro.objects.all()
    context = {'datos': perros}
    return render(request, 'perros_lista_perros1.html', context)


def perro(request, id_perro):
    dato = get_object_or_404(Perro, pk=id_perro)
    comentarios = Comentario.objects.filter(perro=dato)
    context = {'perro': dato, 'comentarios': comentarios}
    return render(request, 'perros_perro.html', context)

def perro2(request, id_perro):
    dato = get_object_or_404(Perro, pk=id_perro)
    comentarios = Comentario.objects.filter(perro=dato)
    context = {'perro': dato, 'comentarios': comentarios}
    return render(request, 'perros_perro1.html', context)


def perro_elimina(request, pk):
    Perro.objects.filter(pk=pk).delete()
    perros = Perro.objects.all()
    return render(request, 'perros_lista_perros.html', {'perro':perros})




def perro_editar(request, pk):
        perro = get_object_or_404(Perro, pk=pk)
        if request.method == "POST":
            form = PerroForm(request.POST, instance=perro)
            if form.is_valid():
                perro = form.save(commit=False)
                perro.save()
                return redirect('perro_perrosform.1.html', pk=perro.pk)
        else:
            form = PerroForm(instance=perro)
            return render(request, 'perro_perrosform.1.html', {'form': form})



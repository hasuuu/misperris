from django.contrib import admin
from .models import Perro, Comentario
# Register your models here.
admin.site.register(Perro)
admin.site.register(Comentario)

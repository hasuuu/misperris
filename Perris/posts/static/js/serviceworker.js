var staticCacheName = 'djangopwa-v1';

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(staticCacheName).then(function(cache) {
      return cache.addAll([
        '/base_layout',
        '/static/js/idb.js',
        '/static/js/idbop.js',
        '/static/css/nivo-slider.css',
        '/static/css/mi-slider.css',
        '/static/css/flexboxgrid.min.css',
        '/static/css/estilos.css',
        '/static/css/fontello.css',
        '/static/css/galeria.css',
        '/static/js/jquery-3.3.1.min.js',
        '/static/js/jquery.nivo.slider.js',
        '/static/js/funciones.js',
        '/static/imagenes/2.jpg',
        '/static/imagenes/Bigotes.jpg',
        '/static/imagenes/Chocolate.jpg',
        '/static/imagenes/crowfunding.jpg',
        '/static/imagenes/logo.1.png',
        '/static/imagenes/logo.png',
        '/static/imagenes/Luna.jpg',
        '/static/imagenes/Maya.jpg',
        '/static/imagenes/Oso.jpg',
        '/static/imagenes/Pexel.jpg',
        '/static/imagenes/rescate.jpg',
        '/static/imagenes/Wifi.jpg',
        '/cliente/ingresar/',
        '/cliente/registrar/',
        '/perros/',
        '/perro/nuevo/',
        '/perros2/',
        '/vista/usuario/',
        '/vista/admin/',
        '/cliente/viewselec/',
        '/cliente/privado/',
        '/cliente/home/',
        '/cliente/form/',
        '/perro/nuevo/',
        '/getdata',
      ]);
    })
  );
});

self.addEventListener('fetch', function(event) {
  var requestUrl = new URL(event.request.url);
    if (requestUrl.origin === location.origin) {
      if ((requestUrl.pathname === '/')) {
        event.respondWith(caches.match('/base_layout'));
        return;
      }
    }
    event.respondWith(
      caches.match(event.request).then(function(response) {
        return response || fetch(event.request);
      })
    );
});
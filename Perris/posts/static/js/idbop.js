var dbPromise = idb.open('Perros-db', 5, function(upgradeDb) {
	upgradeDb.createObjectStore('Perros',{keyPath:'pk'});
});


	//collect latest post from server and store in idb
	fetch('http://127.0.0.1:8000/getdata').then(function(response){
		return response.json();
	}).then(function(jsondata){
		dbPromise.then(function(db){
			var tx = db.transaction('Perros', 'readwrite');
	  		var feedsStore = tx.objectStore('Perros');
	  		for(var key in jsondata){
	  			if (jsondata.hasOwnProperty(key)) {
			    	feedsStore.put(jsondata[key]);	
			  	}
	  		}
		});
	});

	//retrive data from idb and display on page
	var post="";
	dbPromise.then(function(db){
		var tx = db.transaction('Perros', 'readonly');
  		var feedsStore = tx.objectStore('Perros');
  		return feedsStore.openCursor();
	}).then(function logItems(cursor) {
		  if (!cursor) {
		  	document.getElementById('offlinedata').innerHTML=post;
		    return;
		  }
		  for (var field in cursor.value) {
		    	if(field=='fields'){
		    		feedsData=cursor.value[field];
		    		for(var key in feedsData){
		    			if(key =='titulo'){
		    				var titulo = '<h3>'+PerrosData[key]+'</h3>';
		    			}
		    			if(key =='Raza'){
		    				var Raza = PerrosData[key];
		    			}
		    			if(key == 'Descipcion'){
		    				var Descipcion = '<p>'+PerrosData[key]+'</p>';
		    			}	
		    		}
		    		post=post+'<br>'+titulo+'<br>'+Raza+'<br>'+Descipcion+'<br>';
		    	}
		    }
		  return cursor.continue().then(logItems);
		});

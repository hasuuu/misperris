from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.auth.models import User
# Create your models here.

class feed(models.Model):
	id=models.IntegerField(primary_key=True)
	author=models.CharField(max_length=50)
	title=models.CharField(max_length=100)
	body=models.TextField()


	

class Perro(models.Model):
    titulo = models.CharField(max_length=100)
    Raza = models.CharField(max_length=100)
    Descripcion = models.TextField(max_length=200)
    imagen = models.ImageField(upload_to='imagen')
    tiempo_registro = models.DateTimeField(auto_now=True)
    estado = models.CharField(max_length=50)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.titulo



class Comentario(models.Model):
    perro = models.ForeignKey(Perro, on_delete=models.CASCADE)
    texto = models.TextField(max_length=300)

    def __str__(self):
        return self.texto


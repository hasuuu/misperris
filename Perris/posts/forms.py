from django.forms import ModelForm
from django import forms
from .models import Perro, Comentario


class PerroForm(ModelForm):
    class Meta:
        model = Perro
        fields = '__all__'




class ComentarioForm(ModelForm):
    class Meta:
        model = Comentario
        fields = '__all__'



from django.urls import path
from django.conf.urls import url
from . import views
from django.conf.urls import include
urlpatterns = [
    path(r'', views.index,name='index'),
    path(r'base_layout',views.base_layout,name='base_layout'),
    path(r'getdata',views.getdata,name='getdata'),
    url(r'^cliente/ingresar/$', views.ingresar, name='ingresar'),
    url(r'^cliente/registrar/$', views.usuario_nuevo, name='usuario_nuevo'),
    url(r'^perro/nuevo/$', views.Perro_nuevo, name='perro_nuevo'),
    url(r'^perros/detalle/(?P<id_perro>\d+)$', views.perro, name='detalle_perro'),
    url(r'^perros/detalle2/(?P<id_perro>\d+)$', views.perro2, name='detalle_perro'),
    url(r'^perros/$', views.perros, name='perro_listas'),
    url(r'^perros2/$', views.perros2, name='perro_listas'),
    url(r'^vista/usuario/$', views.indexuser, name='vista usuario'),
    url(r'^vista/admin/$', views.indexadmin, name='vista admin'),
    url(r'^cliente/viewselec/$', views.ingresar, name='ingresar'),
    url(r'^cliente/privado/$', views.privado, name='privado'),
    url(r'^cliente/home/$', views.indexuser, name='Home'),
    url(r'^cliente/form/$', views.form, name='Form'),
    url(r'^PerroElim/(?P<pk>[0-9]+)/borrar/$', views.perro_elimina, name='perro_elimina'),
    url(r'^PerroEdit/(?P<pk>[0-9]+)/editar/$', views.perro_editar, name='perro_editar'),
    url(r'^accounts/', include('allauth.urls')),
]